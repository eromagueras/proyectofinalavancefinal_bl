package eduardo.romaguera.dao;

import eduardo.romaguera.bl.entities.Avance.IAvanceDAO;
import eduardo.romaguera.bl.entities.actividadDeportiva.IActividadDeportivaDAO;
import eduardo.romaguera.bl.entities.administrador.IAdministradorDAO;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;
import eduardo.romaguera.bl.entities.atleta.TextoAtletaDAO;
import eduardo.romaguera.bl.entities.canton.ICantonDAO;
import eduardo.romaguera.bl.entities.canton.TextoCantonDAO;
import eduardo.romaguera.bl.entities.distrito.IDistritoDAO;
import eduardo.romaguera.bl.entities.distrito.TextoDistritoDAO;
import eduardo.romaguera.bl.entities.hito.IHitoDAO;
import eduardo.romaguera.bl.entities.metodoPago.IMetodoPagoDAO;
import eduardo.romaguera.bl.entities.pais.IPaisDAO;
import eduardo.romaguera.bl.entities.pais.TextoPaisDAO;
import eduardo.romaguera.bl.entities.provincia.IProvinciaDAO;
import eduardo.romaguera.bl.entities.provincia.TextoProvinciaDAO;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.TextoRetoDAO;
import eduardo.romaguera.bl.entities.retoMatriculado.IRetoMatriculadoDAO;
import eduardo.romaguera.bl.entities.usuario.IUsuarioDAO;
import eduardo.romaguera.bl.entities.usuario.TextoUsuarioDAO;

public class TXTDaoFactory extends DAOFactory {
    public IUsuarioDAO getUsuarioDAO() {
        return new TextoUsuarioDAO();
    }

    public IAtletaDAO getAtletaDAO() {
        return new TextoAtletaDAO();
    }

    @Override
    public IAdministradorDAO getAdministradorDAO() {
        return null;
    }

    public IPaisDAO getPaisDAO() {
        return new TextoPaisDAO();
    }

    public IProvinciaDAO getProvinciaDAO() {
        return new TextoProvinciaDAO();
    }

    public ICantonDAO getCantonDAO() {
        return new TextoCantonDAO();
    }

    public IDistritoDAO getDistritoDAO() {
        return new TextoDistritoDAO();
    }

    public IRetoDAO getRetoDAO() {
        return new TextoRetoDAO();
    }

    @Override
    public IRetoMatriculadoDAO getRetoMatriculadoDAO() {
        return null;
    }


    @Override
    public IActividadDeportivaDAO getActividadDeportivaDAO() {
        return null;
    }

    @Override
    public IHitoDAO getHitoDAO() {
        return null;
    }

    @Override
    public IMetodoPagoDAO getMetodoPagoDAO() {
        return null;
    }

    @Override
    public IAvanceDAO getAvanceDAO() {
        return null;
    }
}
