package eduardo.romaguera.dao;

import eduardo.romaguera.bl.entities.Avance.IAvanceDAO;
import eduardo.romaguera.bl.entities.actividadDeportiva.IActividadDeportivaDAO;
import eduardo.romaguera.bl.entities.administrador.IAdministradorDAO;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;
import eduardo.romaguera.bl.entities.canton.ICantonDAO;
import eduardo.romaguera.bl.entities.distrito.IDistritoDAO;
import eduardo.romaguera.bl.entities.hito.IHitoDAO;
import eduardo.romaguera.bl.entities.metodoPago.IMetodoPagoDAO;
import eduardo.romaguera.bl.entities.pais.IPaisDAO;
import eduardo.romaguera.bl.entities.provincia.IProvinciaDAO;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.retoMatriculado.IRetoMatriculadoDAO;
import eduardo.romaguera.bl.entities.usuario.IUsuarioDAO;

public abstract class DAOFactory {

    public static final int TXT=1;
    public static final int DB=2;

    public static DAOFactory getDaoFactory(int option){
        switch(option){
            case TXT:
                 return new TXTDaoFactory();
            case DB:
                return new DBDaoFactory();
            default:
                return null;
        }
    }

    public abstract IUsuarioDAO getUsuarioDAO();
    public abstract IAtletaDAO getAtletaDAO();
    public abstract IAdministradorDAO getAdministradorDAO();
    public abstract IPaisDAO getPaisDAO();
    public abstract IProvinciaDAO getProvinciaDAO();
    public abstract ICantonDAO getCantonDAO();
    public abstract IDistritoDAO getDistritoDAO();
    public abstract IRetoDAO getRetoDAO();
    public abstract IRetoMatriculadoDAO getRetoMatriculadoDAO();
    public abstract IActividadDeportivaDAO getActividadDeportivaDAO();
    public abstract IHitoDAO getHitoDAO();
    public abstract IMetodoPagoDAO getMetodoPagoDAO();
    public abstract IAvanceDAO getAvanceDAO();

}
