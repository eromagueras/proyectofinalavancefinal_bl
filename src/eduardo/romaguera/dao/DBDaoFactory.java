package eduardo.romaguera.dao;

import eduardo.romaguera.bl.entities.Avance.BDAvanceDAO;
import eduardo.romaguera.bl.entities.Avance.IAvanceDAO;
import eduardo.romaguera.bl.entities.actividadDeportiva.BDActividadDeportivaDAO;
import eduardo.romaguera.bl.entities.actividadDeportiva.IActividadDeportivaDAO;
import eduardo.romaguera.bl.entities.administrador.BDAdministradorDAO;
import eduardo.romaguera.bl.entities.administrador.IAdministradorDAO;
import eduardo.romaguera.bl.entities.atleta.BDAtletaDAO;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;
import eduardo.romaguera.bl.entities.atleta.TextoAtletaDAO;
import eduardo.romaguera.bl.entities.canton.BDCantonDAO;
import eduardo.romaguera.bl.entities.canton.ICantonDAO;
import eduardo.romaguera.bl.entities.canton.TextoCantonDAO;
import eduardo.romaguera.bl.entities.distrito.BDDistritoDAO;
import eduardo.romaguera.bl.entities.distrito.IDistritoDAO;
import eduardo.romaguera.bl.entities.distrito.TextoDistritoDAO;
import eduardo.romaguera.bl.entities.hito.BDHitoDAO;
import eduardo.romaguera.bl.entities.hito.IHitoDAO;
import eduardo.romaguera.bl.entities.metodoPago.BDMetodoPagoDAO;
import eduardo.romaguera.bl.entities.metodoPago.IMetodoPagoDAO;
import eduardo.romaguera.bl.entities.pais.BDPaisDAO;
import eduardo.romaguera.bl.entities.pais.IPaisDAO;
import eduardo.romaguera.bl.entities.pais.TextoPaisDAO;
import eduardo.romaguera.bl.entities.provincia.BDProvinciaDAO;
import eduardo.romaguera.bl.entities.provincia.IProvinciaDAO;
import eduardo.romaguera.bl.entities.provincia.TextoProvinciaDAO;
import eduardo.romaguera.bl.entities.reto.BDRetoDAO;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.TextoRetoDAO;
import eduardo.romaguera.bl.entities.retoMatriculado.BDRetoMatriculadoDAO;
import eduardo.romaguera.bl.entities.retoMatriculado.IRetoMatriculadoDAO;
import eduardo.romaguera.bl.entities.usuario.BDUsuarioDAO;
import eduardo.romaguera.bl.entities.usuario.IUsuarioDAO;
import eduardo.romaguera.bl.entities.usuario.TextoUsuarioDAO;

public class DBDaoFactory extends DAOFactory{

    public IUsuarioDAO getUsuarioDAO() {
        return new BDUsuarioDAO();
    }

    public IAtletaDAO getAtletaDAO() {
        return new BDAtletaDAO();
    }
    public IAdministradorDAO getAdministradorDAO() {
        return new BDAdministradorDAO();
    }
//
//    public IPaisDAO getPaisDAO() {
//        return new BDPaisDAO();
//    }
//
//    public IProvinciaDAO getProvinciaDAO() {
//        return new BDProvinciaDAO();
//    }
//
//    public ICantonDAO getCantonDAO() {
//        return new BDonDAO();
//    }
//
//    public IDistritoDAO getDistritoDAO() {
//        return new BDDistritoDAO();
//    }
//
//    public IRetoDAO getRetoDAO() {
//        return new BDRetoDAO();
//    }

    public IProvinciaDAO getProvinciaDAO() {
        return new BDProvinciaDAO();
    }

    public ICantonDAO getCantonDAO() {
        return new BDCantonDAO();
    }

    public IDistritoDAO getDistritoDAO() {
        return new BDDistritoDAO();
    }

    public IRetoDAO getRetoDAO() {
        return new BDRetoDAO();
    }

    public IRetoMatriculadoDAO getRetoMatriculadoDAO() {
        return new BDRetoMatriculadoDAO();
    }

    public IActividadDeportivaDAO getActividadDeportivaDAO() {
        return new BDActividadDeportivaDAO();
    }

    public IPaisDAO getPaisDAO() {
        return new BDPaisDAO();
    }

    public IHitoDAO getHitoDAO()  {
        return new BDHitoDAO();
    }

    public IMetodoPagoDAO getMetodoPagoDAO() {
        return new BDMetodoPagoDAO();
    }
    public IAvanceDAO getAvanceDAO() {
        return new BDAvanceDAO();
    }

}
