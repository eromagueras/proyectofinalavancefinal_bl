package eduardo.romaguera.bl.entities.usuario;

import eduardo.romaguera.bl.entities.administrador.Administrador;
import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.dl.txt.Conector;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TextoUsuarioDAO implements IUsuarioDAO{

    private final String ADMINISTRADOR_BD ="src\\eduardo\\romaguera\\dl\\txt\\AdministradorBD.txt";
    private final String ATLETAS_BD ="src\\eduardo\\romaguera\\dl\\txt\\AtletasBD.txt";



        public Usuario verificarAcceso(Usuario usuarioBuscado) throws Exception {
            ArrayList<Usuario> listaAtletas;
            ArrayList<Usuario> listaAdministradores;
            listaAtletas = listarAtletas();
            listaAdministradores = listarAdministradores();
            for (Usuario usuarioAd : listaAdministradores) {
                if (usuarioAd.equalsID(usuarioBuscado)) {
                    return usuarioAd;
                } else {
                    for (Usuario usuarioAt : listaAtletas) {
                        if (usuarioAt.equalsID(usuarioBuscado)) {
                            return usuarioAt;
                        } else {
                            usuarioBuscado.setTipoUsuario("Error");
                            return usuarioBuscado;
                        }
                    }
                }

            }
            usuarioBuscado.setTipoUsuario("Error");
            return usuarioBuscado;
        }

    public ArrayList<Usuario> listarAdministradores() {
        ArrayList<Usuario> lista = new ArrayList<>();
        try{
            FileReader reader = new FileReader(ADMINISTRADOR_BD);
            BufferedReader buffer = new BufferedReader(reader);
            String datos;
            String[] infoAdministrador;
            Usuario usuario;

            while((datos = buffer.readLine()) != null){
                infoAdministrador = datos.split(",");
                Pais Pais = new Pais(infoAdministrador[3]);
                usuario = new Usuario(infoAdministrador[0],infoAdministrador[1],infoAdministrador[2], Pais,infoAdministrador[4],infoAdministrador[5],infoAdministrador[6]);
                lista.add(usuario);
            }

        }catch(IOException e){
            e.printStackTrace();
        }
        return lista;
    }

    public ArrayList<Usuario> listarAtletas() {
        ArrayList<Usuario> lista = new ArrayList<>();
        try{
            FileReader reader = new FileReader(ATLETAS_BD);
            BufferedReader buffer = new BufferedReader(reader);
            String datos;
            String[] infoAtleta;
            Usuario usuario;

            while((datos = buffer.readLine()) != null){
                infoAtleta = datos.split(",");
                Pais Pais = new Pais(infoAtleta[3]);
                usuario = new Usuario(infoAtleta[0],infoAtleta[1],infoAtleta[2],Pais,infoAtleta[4],infoAtleta[5],infoAtleta[6]);
                lista.add(usuario);
            }

        }catch(IOException e){
            e.printStackTrace();
        }
        return lista;
    }

}
