package eduardo.romaguera.bl.entities.usuario;

import eduardo.romaguera.bl.entities.pais.Pais;

import java.util.Objects;

public class Usuario {
    private String nombre;
    private String apellidos;
    private String identificacion;
    private Pais pais;
    private String email;
    private String contrasena;
    private String tipoUsuario;
    private String avatar;

    public Usuario() {
    }

    public Usuario(String nombre) {
        this.nombre = nombre;
    }

    public Usuario(String nombre, String apellidos, String identificacion, Pais pais, String email, String contrasena, String tipoUsuario) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.identificacion = identificacion;
        this.pais = pais;
        this.email = email;
        this.contrasena = contrasena;
        this.tipoUsuario = tipoUsuario;
    }

    public Usuario(String nombre, String apellidos, String identificacion, Pais pais, String email, String contrasena, String tipoUsuario, String avatar) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.identificacion = identificacion;
        this.pais = pais;
        this.email = email;
        this.contrasena = contrasena;
        this.tipoUsuario = tipoUsuario;
        this.avatar = avatar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", pais=" + pais +
                ", email='" + email + '\'' +
                ", contrasena='" + contrasena + '\'' +
                ", tipoUsuario='" + tipoUsuario + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return Objects.equals(getNombre(), usuario.getNombre()) && Objects.equals(getApellidos(), usuario.getApellidos()) && Objects.equals(getIdentificacion(), usuario.getIdentificacion()) && Objects.equals(getPais(), usuario.getPais()) && Objects.equals(getEmail(), usuario.getEmail()) && Objects.equals(getContrasena(), usuario.getContrasena()) && Objects.equals(getTipoUsuario(), usuario.getTipoUsuario());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNombre(), getApellidos(), getIdentificacion(), getPais(), getEmail(), getContrasena(), getTipoUsuario());
    }

    public boolean equalsID(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return Objects.equals(getEmail(), usuario.getEmail()) && Objects.equals(getContrasena(), usuario.getContrasena());
    }

}
