package eduardo.romaguera.bl.entities.usuario;


import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.usuario.Usuario;
import eduardo.romaguera.dl.db.ConectorBD;
import eduardo.romaguera.dl.txt.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDUsuarioDAO implements IUsuarioDAO {
    private String sqlQuery;

    /**
     * Metodo que verifica el acceso de un usuario (login)
     * @param usuarioBuscado
     * @return String "Good"
     * @throws Exception
     */
    public Usuario verificarAcceso(Usuario usuarioBuscado) throws Exception {
        ArrayList<Usuario> listaAtletas;
        ArrayList<Usuario> listaAdministradores;
        listaAtletas = listarAtletas();
        listaAdministradores = listarAdministradores();
        for (Usuario usuarioAd : listaAdministradores) {
            if (usuarioAd.equalsID(usuarioBuscado)) {
                return usuarioAd;
            }
        }
        for (Usuario usuarioAd : listaAtletas) {
            if (usuarioAd.equalsID(usuarioBuscado)) {
                return usuarioAd;
            }
        }
        usuarioBuscado.setTipoUsuario("Error");
        return usuarioBuscado;
    }

    /**
     * Metodo que lista usuarios Administradores en la BD
     * @param
     * @return ArrayList<Usuario>
     * @throws Exception
     */
    public ArrayList<Usuario> listarAdministradores() throws Exception {
        ArrayList<Usuario> lista = new ArrayList<>();

        sqlQuery="SELECT nombre, apellidos, identificacion, pais, email, contrasena, tipoUsuario FROM Administradores";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Usuario usuario = new Usuario();
            usuario.setNombre(rs.getString("nombre"));
            usuario.setApellidos(rs.getString("apellidos"));
            usuario.setIdentificacion(rs.getString("identificacion"));
            Pais pais = new Pais();
            pais.setNombre(rs.getString("pais"));
            usuario.setPais(pais);
            usuario.setEmail(rs.getString("email"));
            usuario.setContrasena(rs.getString("contrasena"));
            usuario.setTipoUsuario(rs.getString("tipoUsuario"));
            lista.add(usuario);
        }
        return lista;
    }

    /**
     * Metodo que lista usuarios Atletas en la BD
     * @param
     * @return ArrayList<Usuario>
     * @throws Exception
     */
    public ArrayList<Usuario> listarAtletas() throws Exception {
        ArrayList<Usuario> lista = new ArrayList<>();

        sqlQuery="SELECT * FROM Atletas";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Usuario usuario = new Usuario();
            usuario.setNombre(rs.getString("nombre"));
            usuario.setApellidos(rs.getString("apellidos"));
            usuario.setIdentificacion(rs.getString("identificacion"));
            Pais pais = new Pais();
            pais.setNombre("Vacio");
            pais.setNombre(rs.getString("pais"));
            usuario.setPais(pais);
            usuario.setEmail(rs.getString("email"));
            usuario.setContrasena(rs.getString("contrasena"));
            usuario.setTipoUsuario(rs.getString("tipoUsuario"));
            usuario.setAvatar(rs.getString("avatar"));
            lista.add(usuario);
        }
        return lista;
    }
}