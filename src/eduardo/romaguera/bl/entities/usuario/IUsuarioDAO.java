package eduardo.romaguera.bl.entities.usuario;

import eduardo.romaguera.bl.entities.atleta.Atleta;

import java.util.ArrayList;

public interface IUsuarioDAO {
    Usuario verificarAcceso(Usuario usuarioBuscado) throws Exception;

    ArrayList<Usuario> listarAdministradores() throws Exception;

    ArrayList<Usuario> listarAtletas() throws Exception;
}
