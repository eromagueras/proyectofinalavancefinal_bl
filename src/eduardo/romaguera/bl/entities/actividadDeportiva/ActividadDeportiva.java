package eduardo.romaguera.bl.entities.actividadDeportiva;

import java.util.Objects;

public class ActividadDeportiva {
    private String codigo;
    private String nombre;
    private String icono;

    public ActividadDeportiva() {
    }

    public ActividadDeportiva(String codigo, String nombre, String icono) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.icono = icono;
    }

    public ActividadDeportiva(String nombre, String icono) {
        this.nombre = nombre;
        this.icono = icono;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActividadDeportiva that = (ActividadDeportiva) o;
        return getCodigo() == that.getCodigo() && Objects.equals(getNombre(), that.getNombre()) && Objects.equals(getIcono(), that.getIcono());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo(), getNombre(), getIcono());
    }

    @Override
    public String toString() {
        return "ActividadDeportiva{" +
                "codigo='" + codigo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", icono='" + icono + '\'' +
                '}';
    }

}
