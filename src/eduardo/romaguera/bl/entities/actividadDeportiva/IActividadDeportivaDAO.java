package eduardo.romaguera.bl.entities.actividadDeportiva;

import eduardo.romaguera.bl.entities.reto.Reto;

import java.util.ArrayList;

public interface IActividadDeportivaDAO {
    String registrarActividadDeportiva(ActividadDeportiva nuevaActividadDeportiva) throws Exception;
    ArrayList<ActividadDeportiva> listarActividadDeportiva() throws Exception;
}
