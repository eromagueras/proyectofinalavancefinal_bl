package eduardo.romaguera.bl.entities.actividadDeportiva;


import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.bl.logic.ActividadDeportivaGestor;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDActividadDeportivaDAO implements IActividadDeportivaDAO {
    private String sqlQuery;

    /**
     * Metodo que registra una actividad deportiva en la BD
     * @param nuevaActividadDeportiva
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarActividadDeportiva(ActividadDeportiva nuevaActividadDeportiva) throws Exception {
        sqlQuery = "INSERT INTO ActividadesDeportivas "+
            "(nombre, icono)"+
            "VALUES('"+
                nuevaActividadDeportiva.getNombre()+"', '"+
                nuevaActividadDeportiva.getIcono()
            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

    /**
     * Metodo que lista Actividad Deportiva en la BD
     * @param
     * @return ArrayList<ActividadDeportiva>
     * @throws Exception
     */
    @Override
    public ArrayList<ActividadDeportiva> listarActividadDeportiva() throws Exception {
        ArrayList<ActividadDeportiva> lista = new ArrayList<>();
        sqlQuery="SELECT * FROM ActividadesDeportivas";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            ActividadDeportiva actividadDeportiva = new ActividadDeportiva();
            actividadDeportiva.setNombre(rs.getString("nombre"));
            actividadDeportiva.setCodigo(rs.getString("codigo"));
            actividadDeportiva.setIcono(rs.getString("icono"));
            lista.add(actividadDeportiva);
        }
        return lista;
    }

}
