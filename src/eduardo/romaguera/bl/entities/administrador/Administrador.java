package eduardo.romaguera.bl.entities.administrador;

import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.usuario.Usuario;

public class Administrador extends Usuario {
    public Administrador(String nombre, String apellidos, String identificacion, Pais pais, String email, String contrasena, String tipoUsuario) {
        super(nombre, apellidos, identificacion, pais, email, contrasena, "Administrador");
    }

    public Administrador(String nombre, String apellidos, String identificacion, Pais pais, String email, String contrasena) {
        super(nombre, apellidos, identificacion, pais, email, contrasena, "Administrador");
    }

    public Administrador() {
    }

}
