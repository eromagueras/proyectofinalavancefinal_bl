package eduardo.romaguera.bl.entities.administrador;

import eduardo.romaguera.bl.entities.atleta.Atleta;

public interface IAdministradorDAO {
        String registrarAdministrador(Administrador nuevoAdministrador) throws Exception;
    }
