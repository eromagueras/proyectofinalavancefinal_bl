package eduardo.romaguera.bl.entities.administrador;

import eduardo.romaguera.dl.db.ConectorBD;

public class BDAdministradorDAO implements IAdministradorDAO {
    private String sqlQuery;

    /**
     * Metodo que registra un Administrador en la BD
     * @param nuevoAdministrador
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarAdministrador(Administrador nuevoAdministrador) throws Exception {
        sqlQuery = "INSERT INTO Administradores "+
            "(nombre, apellidos, identificacion, pais, email, contrasena, tipoUsuario)"+
            "VALUES('"+
                nuevoAdministrador.getNombre()+"', '"+
                nuevoAdministrador.getApellidos()+"', '"+
                nuevoAdministrador.getIdentificacion()+"', '"+
                nuevoAdministrador.getPais().toStringSV()+"', '"+
                nuevoAdministrador.getEmail()+"', '"+
                nuevoAdministrador.getContrasena()+"', '"+
            "Administrador"
            +"')";
        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

}
