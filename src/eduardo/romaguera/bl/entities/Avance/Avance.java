package eduardo.romaguera.bl.entities.Avance;

import eduardo.romaguera.bl.entities.actividadDeportiva.ActividadDeportiva;

import java.util.Objects;

public class Avance {
    private int codigo;
    private int codigoM;
    private ActividadDeportiva actividadDeportiva;
    private int kilometros;

    public Avance() {
    }

    public Avance(int codigo, int codigoM, ActividadDeportiva actividadDeportiva, int kilometros) {
        this.codigo = codigo;
        this.codigoM = codigoM;
        this.actividadDeportiva = actividadDeportiva;
        this.kilometros = kilometros;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigoM() {
        return codigoM;
    }

    public void setCodigoM(int codigoM) {
        this.codigoM = codigoM;
    }

    public ActividadDeportiva getActividadDeportiva() {
        return actividadDeportiva;
    }

    public void setActividadDeportiva(ActividadDeportiva actividadDeportiva) {
        this.actividadDeportiva = actividadDeportiva;
    }

    public int getKilometros() {
        return kilometros;
    }

    public void setKilometros(int kilometros) {
        this.kilometros = kilometros;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Avance avance = (Avance) o;
        return getCodigo() == avance.getCodigo() && getCodigoM() == avance.getCodigoM() && getKilometros() == avance.getKilometros() && Objects.equals(getActividadDeportiva(), avance.getActividadDeportiva());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo(), getCodigoM(), getActividadDeportiva(), getKilometros());
    }

    @Override
    public String toString() {
        return "Avance{" +
                "codigo=" + codigo +
                ", codigoM=" + codigoM +
                ", actividadDeportiva=" + actividadDeportiva +
                ", kilometros=" + kilometros +
                '}';
    }
}
