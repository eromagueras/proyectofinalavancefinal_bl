package eduardo.romaguera.bl.entities.Avance;


import eduardo.romaguera.bl.entities.actividadDeportiva.ActividadDeportiva;
import eduardo.romaguera.bl.entities.hito.Hito;
import eduardo.romaguera.bl.entities.hito.IHitoDAO;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDAvanceDAO implements IAvanceDAO {
    private String sqlQuery;
    private String sqlQuery2;

    /**
     * Metodo que registra un Avance en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarAvance(Avance nuevo) throws Exception {
        sqlQuery = "INSERT INTO Avances "+
            "(codigoM, kilometros, actividadDeportiva)"+
            "VALUES('"+
                nuevo.getCodigoM()+"', '"+
                nuevo.getKilometros()+"', '"+
                nuevo.getActividadDeportiva().getNombre()
            +"')";

        sqlQuery2 = "UPDATE RetosMatriculados SET avance = "+
                nuevo.getKilometros()+" "+
                "Where RetosMatriculados.codigoM = "+
                nuevo.getCodigoM()
                +"";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery2);
        return "Good";
    }

    /**
     * Metodo que lista Avances en la BD
     * @param
     * @return ArrayList<Avance>
     * @throws Exception
     */
    public ArrayList<Avance> listarAvances() throws Exception {
        ArrayList<Avance> lista = new ArrayList<>();
        sqlQuery="SELECT * FROM Avances";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Avance avance = new Avance();
            avance.setCodigoM(Integer.valueOf(rs.getString("codigoM")));
            avance.setKilometros(Integer.valueOf(rs.getString("kilometros")));
            ActividadDeportiva actividad = new ActividadDeportiva();
            actividad.setNombre(rs.getString("descripcion"));
            avance.setActividadDeportiva(actividad);
            lista.add(avance);
        }
        return lista;
    }

}
