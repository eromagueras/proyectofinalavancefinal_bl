package eduardo.romaguera.bl.entities.Avance;

import eduardo.romaguera.bl.entities.hito.Hito;

import java.util.ArrayList;

public interface IAvanceDAO {
    String registrarAvance(Avance nuevo) throws Exception;
    ArrayList<Avance> listarAvances() throws Exception;
}
