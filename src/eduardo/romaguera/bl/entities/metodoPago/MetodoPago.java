package eduardo.romaguera.bl.entities.metodoPago;

import java.time.LocalDate;
import java.util.Objects;

public class MetodoPago {
    private int codigo;
    private String nombre;
    private double numeroTarjeta;
    private String proveedor;
    private LocalDate fecha;
    private int cvv;
    private String email;

    public MetodoPago() {
    }

    public MetodoPago(int codigo, String nombre, double numeroTarjeta, String proveedor, LocalDate fecha, int cvv, String email) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.numeroTarjeta = numeroTarjeta;
        this.proveedor = proveedor;
        this.fecha = fecha;
        this.cvv = cvv;
        this.email = email;
    }

    public MetodoPago(String nombre, double numeroTarjeta, String proveedor, LocalDate fecha, int cvv) {
        this.nombre = nombre;
        this.numeroTarjeta = numeroTarjeta;
        this.proveedor = proveedor;
        this.fecha = fecha;
        this.cvv = cvv;
    }

    public MetodoPago(String nombre, double numeroTarjeta, String proveedor, LocalDate fecha, int cvv, String email) {
        this.nombre = nombre;
        this.numeroTarjeta = numeroTarjeta;
        this.proveedor = proveedor;
        this.fecha = fecha;
        this.cvv = cvv;
        this.email = email;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(double numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetodoPago that = (MetodoPago) o;
        return getCodigo() == that.getCodigo() && Double.compare(that.getNumeroTarjeta(), getNumeroTarjeta()) == 0 && getCvv() == that.getCvv() && Objects.equals(getNombre(), that.getNombre()) && Objects.equals(getProveedor(), that.getProveedor()) && Objects.equals(getFecha(), that.getFecha()) && Objects.equals(getEmail(), that.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo(), getNombre(), getNumeroTarjeta(), getProveedor(), getFecha(), getCvv(), getEmail());
    }

    @Override
    public String toString() {
        return "MetodoPago{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", numeroTarjeta=" + numeroTarjeta +
                ", proveedor='" + proveedor + '\'' +
                ", fecha=" + fecha +
                ", cvv=" + cvv +
                ", email='" + email + '\'' +
                '}';
    }
}
