package eduardo.romaguera.bl.entities.metodoPago;

import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.bl.entities.usuario.Usuario;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;

public class BDMetodoPagoDAO implements IMetodoPagoDAO {
    private String sqlQuery;

    private int codigo;
    private String nombre;
    private int numeroTarjeta;
    private String proveedor;
    private LocalDate fecha;
    private int cvv;


    /**
     * Metodo que registra un MetodoPago en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarMetodoPago(MetodoPago nuevo) throws Exception {
        sqlQuery = "INSERT INTO MetodosPago "+
            "(nombre, numeroTarjeta, cvv, proveedor, fecha, correo)"+
            "VALUES('"+
                nuevo.getNombre()+"', '"+
                nuevo.getNumeroTarjeta()+"', '"+
                nuevo.getCvv()+"', '"+
                nuevo.getProveedor()+"', '"+
                nuevo.getFecha()+"', '"+
                nuevo.getEmail()
            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

    /**
     * Metodo que lista Metodos de pago en la BD
     * @param usuario Usuario
     * @return ArrayList<MetodoPago>
     * @throws Exception
     */
    public ArrayList<MetodoPago> listarMetodoPago(Usuario usuario) throws Exception {
        ArrayList<MetodoPago> lista = new ArrayList<>();
        String email = usuario.getEmail();
        sqlQuery="SELECT * FROM MetodosPago WHERE correo= '"+email+"'";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            MetodoPago metodoPago = new MetodoPago();
            metodoPago.setNombre(rs.getString("nombre"));
            metodoPago.setCodigo(Integer.valueOf(rs.getString("codigo")));
            metodoPago.setNumeroTarjeta(Double.parseDouble(rs.getString("numeroTarjeta")));
            metodoPago.setCvv(Integer.valueOf(rs.getString("cvv")));
            metodoPago.setProveedor(rs.getString("proveedor"));
            metodoPago.setFecha(LocalDate.parse(rs.getString("fecha")));
            lista.add(metodoPago);
        }
        return lista;
    }

}
