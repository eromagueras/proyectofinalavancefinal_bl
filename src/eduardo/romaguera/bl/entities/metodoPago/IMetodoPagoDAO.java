package eduardo.romaguera.bl.entities.metodoPago;

import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.bl.entities.usuario.Usuario;

import java.util.ArrayList;

public interface IMetodoPagoDAO {
    String registrarMetodoPago(MetodoPago nuevo) throws Exception;
    ArrayList<MetodoPago> listarMetodoPago(Usuario usuario) throws Exception;
}
