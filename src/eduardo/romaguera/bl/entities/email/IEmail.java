package eduardo.romaguera.bl.entities.email;

public interface IEmail {
    void sendEmail(Email email) throws Exception;
}
