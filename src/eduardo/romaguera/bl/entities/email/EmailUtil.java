package eduardo.romaguera.bl.entities.email;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailUtil implements IEmail{

    /**
     * Metodo que envía un email
     * @param email
     * @return void
     * @throws Exception
     */
    @Override
    public void sendEmail(Email email) throws Exception {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        String emailAccount = "sochisoftware2021@gmail.com";
        String password = "Soch1sof7t!";

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailAccount.toString(), password);
            }
        });
        Message message = prepareMessage(session, emailAccount, email.getDestinatario(), email.getAsunto(), email.getContenido());
        Transport.send(message);
    }

    /**
     * Metodo que prepara el mensaje a ser enviado
     * @param session
     * @return message
     * @throws Exception
     */
    private Message prepareMessage(Session session, String emailAccount, String destinatario, String asunto, String mensaje) throws MessagingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(emailAccount));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
        message.setSubject(asunto);
        message.setText(mensaje);
        return message;
    }
}
