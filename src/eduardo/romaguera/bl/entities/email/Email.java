package eduardo.romaguera.bl.entities.email;

import java.util.Objects;

public class Email {
    private String destinatario;
    private String asunto;
    private String contenido;

    public Email() {
    }

    public Email(String destinatario, String asunto, String contenido) {
        this.destinatario = destinatario;
        this.asunto = asunto;
        this.contenido = contenido;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return Objects.equals(getDestinatario(), email.getDestinatario()) && Objects.equals(getAsunto(), email.getAsunto()) && Objects.equals(getContenido(), email.getContenido());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDestinatario(), getAsunto(), getContenido());
    }

    @Override
    public String toString() {
        return "Email{" +
                "destinatario='" + destinatario + '\'' +
                ", asunto='" + asunto + '\'' +
                ", contenido='" + contenido + '\'' +
                '}';
    }
}