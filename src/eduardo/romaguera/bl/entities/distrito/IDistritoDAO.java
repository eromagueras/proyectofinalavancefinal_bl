package eduardo.romaguera.bl.entities.distrito;

import eduardo.romaguera.bl.entities.canton.Canton;

import java.util.ArrayList;

public interface IDistritoDAO {

    ArrayList<Distrito> listarDistritos() throws Exception;
    String registrarDistrito(Distrito nuevo) throws Exception;
}
