package eduardo.romaguera.bl.entities.distrito;

import java.util.Objects;

public class Distrito {
    private String codigo;
    private String nombre;
    private String NombreCanton;

    public Distrito() {
    }

    public Distrito(String nombre, String nombreCanton) {
        this.nombre = nombre;
        NombreCanton = nombreCanton;
    }

    public Distrito(String codigo, String nombre, String nombreCanton) {
        this.codigo = codigo;
        this.nombre = nombre;
        NombreCanton = nombreCanton;
    }

    public Distrito(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCanton() {
        return NombreCanton;
    }

    public void setNombreCanton(String nombreCanton) {
        NombreCanton = nombreCanton;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Distrito distrito = (Distrito) o;
        return Objects.equals(getCodigo(), distrito.getCodigo()) && Objects.equals(getNombre(), distrito.getNombre()) && Objects.equals(getNombreCanton(), distrito.getNombreCanton());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo(), getNombre(), getNombreCanton());
    }

    @Override
    public String toString() {
        return "Distrito{" +
                "codigo='" + codigo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", NombreCanton='" + NombreCanton + '\'' +
                '}';
    }

    public String toStringSV() {
        return getNombre();
    }
}
