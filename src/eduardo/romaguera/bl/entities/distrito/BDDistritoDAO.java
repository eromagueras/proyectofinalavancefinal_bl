package eduardo.romaguera.bl.entities.distrito;

import eduardo.romaguera.bl.entities.canton.Canton;
import eduardo.romaguera.bl.entities.canton.ICantonDAO;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDDistritoDAO implements IDistritoDAO{
    private String sqlQuery;

    /**
     * Metodo que lista Distritos en la BD
     * @param
     * @return ArrayList<Distrito>
     * @throws Exception
     */
    @Override
    public ArrayList<Distrito> listarDistritos() throws Exception {
        ArrayList<Distrito> lista = new ArrayList<>();
        sqlQuery="SELECT * FROM Distritos";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Distrito distrito = new Distrito();
            distrito.setNombre(rs.getString("nombre"));
            distrito.setNombreCanton(rs.getString("nombreCanton"));
            distrito.setCodigo(rs.getString("codigo"));
            lista.add(distrito);
        }
        return lista;
    }

    /**
     * Metodo que registra un Distrito en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarDistrito(Distrito nuevo) throws Exception {
        sqlQuery = "INSERT INTO Distritos "+
            "(nombre, nombreCanton)"+
            "VALUES('"+
                nuevo.getNombre()+"', '"+
                nuevo.getNombreCanton()
//                +"', '"+

            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

}
