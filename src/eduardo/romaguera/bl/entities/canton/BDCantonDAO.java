package eduardo.romaguera.bl.entities.canton;

import eduardo.romaguera.bl.entities.pais.IPaisDAO;
import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDCantonDAO implements ICantonDAO {
    private String sqlQuery;

    /**
     * Metodo que lista Cantones en la BD
     * @param
     * @return ArrayList<Canton>
     * @throws Exception
     */
    @Override
    public ArrayList<Canton> listarCantones() throws Exception {
        ArrayList<Canton> lista = new ArrayList<>();
        sqlQuery="SELECT * FROM Cantones";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Canton canton = new Canton();
            canton.setNombre(rs.getString("nombre"));
            canton.setNombreProvincia(rs.getString("nombreProvincia"));
            canton.setNombreProvincia(rs.getString("nombreProvincia"));
            canton.setCodigo((Integer.valueOf(rs.getString("codigo"))));
            lista.add(canton);
        }
        return lista;
    }

    /**
     * Metodo que registra un Canton en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarCanton(Canton nuevo) throws Exception {
        sqlQuery = "INSERT INTO Cantones "+
            "(nombre, nombreProvincia)"+
            "VALUES('"+
                nuevo.getNombre()+"', '"+
                nuevo.getNombreProvincia()
//                +"', '"+
            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

}
