package eduardo.romaguera.bl.entities.canton;

import eduardo.romaguera.bl.entities.pais.Pais;

import java.util.ArrayList;

public interface ICantonDAO {

    ArrayList<Canton> listarCantones() throws Exception;
    String registrarCanton(Canton nuevo) throws Exception;
}
