package eduardo.romaguera.bl.entities.canton;

import java.util.Objects;

public class Canton {
    private int codigo;
    private String nombre;
    private String nombreProvincia;

    public Canton() {
    }

    public Canton(String nombre, String nombreProvincia) {
        this.nombre = nombre;
        this.nombreProvincia = nombreProvincia;
    }

    public Canton(int codigo, String nombre, String nombreProvincia) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.nombreProvincia = nombreProvincia;
    }

    public Canton(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreProvincia() {
        return nombreProvincia;
    }

    public void setNombreProvincia(String nombreProvincia) {
        this.nombreProvincia = nombreProvincia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Canton canton = (Canton) o;
        return getCodigo() == canton.getCodigo() && Objects.equals(getNombre(), canton.getNombre()) && Objects.equals(getNombreProvincia(), canton.getNombreProvincia());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo(), getNombre(), getNombreProvincia());
    }

    @Override
    public String toString() {
        return "Canton{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", nombreProvincia='" + nombreProvincia + '\'' +
                '}';
    }

    public String toStringSV() {
        return getNombre();
    }
}