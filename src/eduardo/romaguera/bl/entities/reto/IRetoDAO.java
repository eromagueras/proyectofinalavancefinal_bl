package eduardo.romaguera.bl.entities.reto;

import eduardo.romaguera.bl.entities.provincia.Provincia;

import java.util.ArrayList;

public interface IRetoDAO {
    String registrarReto(Reto nuevoReto) throws Exception;
    ArrayList<Reto> listarRetos() throws Exception;

}
