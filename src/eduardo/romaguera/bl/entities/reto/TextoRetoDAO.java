package eduardo.romaguera.bl.entities.reto;

import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TextoRetoDAO implements IRetoDAO {

    private final String RETO_BD ="src\\eduardo\\romaguera\\dl\\txt\\RetosBD.txt";

    public String registrarReto(Reto nuevoReto) throws Exception {
        try{
            FileWriter writer = new FileWriter(RETO_BD,true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(nuevoReto.toStringCSV());
            buffer.newLine();
            buffer.close();
            return "Good";
        }catch(IOException e){
            e.printStackTrace();
        }

        return "Error";
    }

    @Override
    public ArrayList<Reto> listarRetos() throws Exception {
        return null;
    }

}