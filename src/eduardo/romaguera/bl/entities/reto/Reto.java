package eduardo.romaguera.bl.entities.reto;

import eduardo.romaguera.bl.entities.hito.Hito;
import eduardo.romaguera.bl.entities.pais.Pais;

import java.util.ArrayList;
import java.util.Objects;

public class Reto {
    private String nombre;
    private String codigo;
    private double kilometros;
    private Pais pais;
    private String medalla;
    private String fotografia;
    private String descripcion;
    private ArrayList<Hito> hitos;

    public Reto() {
    }

    public Reto(String nombre, String codigo, double kilometros, Pais pais, String medalla, String fotografia, String descripcion) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.kilometros = kilometros;
        this.pais = pais;
        this.medalla = medalla;
        this.fotografia = fotografia;
        this.descripcion = descripcion;
    }

    public Reto(String nombre, String codigo, double kilometros, Pais pais, String medalla, String fotografia, String descripcion, ArrayList<Hito> hitos) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.kilometros = kilometros;
        this.pais = pais;
        this.medalla = medalla;
        this.fotografia = fotografia;
        this.descripcion = descripcion;
        this.hitos = hitos;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getKilometros() {
        return kilometros;
    }

    public void setKilometros(double kilometros) {
        this.kilometros = kilometros;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public String getMedalla() {
        return medalla;
    }

    public void setMedalla(String medalla) {
        this.medalla = medalla;
    }

    public String getFotografia() {
        return fotografia;
    }

    public void setFotografia(String fotografia) {
        this.fotografia = fotografia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public ArrayList<Hito> getHitos() {
        return hitos;
    }

    public void setHitos(ArrayList<Hito> hitos) {
        this.hitos = hitos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reto reto = (Reto) o;
        return Double.compare(reto.getKilometros(), getKilometros()) == 0 && Objects.equals(getNombre(), reto.getNombre()) && Objects.equals(getCodigo(), reto.getCodigo()) && Objects.equals(getPais(), reto.getPais()) && Objects.equals(getMedalla(), reto.getMedalla()) && Objects.equals(getFotografia(), reto.getFotografia()) && Objects.equals(getDescripcion(), reto.getDescripcion()) && Objects.equals(getHitos(), reto.getHitos());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNombre(), getCodigo(), getKilometros(), getPais(), getMedalla(), getFotografia(), getDescripcion(), getHitos());
    }

    @Override
    public String toString() {
        return "Reto{" +
                "nombre='" + nombre + '\'' +
                ", codigo='" + codigo + '\'' +
                ", kilometros=" + kilometros +
                ", pais=" + pais +
                ", medalla='" + medalla + '\'' +
                ", fotografia='" + fotografia + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", hitos=" + hitos +
                '}';
    }

    public String toStringCSV(){
        return nombre + ","+ codigo + ","+ kilometros + ","+ pais.getNombre() + ","+ medalla + ","+ fotografia + ","+ descripcion;
    }
}
