package eduardo.romaguera.bl.entities.reto;


import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;
import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.provincia.Provincia;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDRetoDAO implements IRetoDAO {
    private String sqlQuery;

    /**
     * Metodo que registra un Reto en la BD
     * @param nuevoReto
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarReto(Reto nuevoReto) throws Exception {
//        sqlQuery = "INSERT INTO Atletas (nombre, apellidos, identificacion, pais) VALUES('Eduardo', 'Romaguera', 1111, 'Costa Rica')";
        sqlQuery = "INSERT INTO Retos "+
            "(nombre, codigo, kilometros, pais, medalla, fotografia, descripcion)"+
            "VALUES('"+
                nuevoReto.getNombre()+"', '"+
                nuevoReto.getCodigo()+"', '"+
                nuevoReto.getKilometros()+"', '"+
                nuevoReto.getPais().toStringSV()+"', '"+
                nuevoReto.getMedalla()+"', '"+
                nuevoReto.getFotografia()+"', '"+
                nuevoReto.getDescripcion()
            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

    /**
     * Metodo que lista Retos en la BD
     * @param
     * @return ArrayList<Reto>
     * @throws Exception
     */
    public ArrayList<Reto> listarRetos() throws Exception {
        ArrayList<Reto> lista = new ArrayList<>();
        sqlQuery="SELECT * FROM Retos";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Reto reto = new Reto();
            reto.setNombre(rs.getString("nombre"));
            reto.setCodigo(rs.getString("codigo"));
            reto.setKilometros(Double.valueOf(rs.getString("kilometros")));
            Pais pais = new Pais(rs.getString("pais"));
            reto.setPais(pais);
            reto.setMedalla(rs.getString("medalla"));
            reto.setFotografia(rs.getString("fotografia"));
            reto.setDescripcion(rs.getString("descripcion"));
            lista.add(reto);
        }
        return lista;
    }

}
