package eduardo.romaguera.bl.entities.hito;

import eduardo.romaguera.bl.entities.reto.Reto;

import java.util.ArrayList;

public interface IHitoDAO {
    String registrarHito(Hito nuevo) throws Exception;
    ArrayList<Hito> listarHitos() throws Exception;
}
