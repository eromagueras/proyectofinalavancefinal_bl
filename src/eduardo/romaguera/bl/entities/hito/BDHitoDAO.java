package eduardo.romaguera.bl.entities.hito;


import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDHitoDAO implements IHitoDAO {
    private String sqlQuery;

    /**
     * Metodo que registra un Hito en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarHito(Hito nuevo) throws Exception {
        sqlQuery = "INSERT INTO Hitos "+
            "(nombre, kilometros, descripcion, link, imagen, reto)"+
            "VALUES('"+
                nuevo.getNombre()+"', '"+
                nuevo.getKilometros()+"', '"+
                nuevo.getDescripcion()+"', '"+
                nuevo.getLink()+"', '"+
                nuevo.getImagen()+"', '"+
                nuevo.getReto()
            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

    /**
     * Metodo que lista Hitos en la BD
     * @param
     * @return ArrayList<Hito>
     * @throws Exception
     */
    public ArrayList<Hito> listarHitos() throws Exception {
        ArrayList<Hito> lista = new ArrayList<>();
        sqlQuery="SELECT * FROM Hitos";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Hito hito = new Hito();
            hito.setCodigo(Integer.valueOf(rs.getString("codigo")));
            hito.setNombre(rs.getString("nombre"));
            hito.setKilometros(Double.valueOf(rs.getString("kilometros")));
            hito.setDescripcion(rs.getString("descripcion"));
            hito.setLink(rs.getString("link"));
            hito.setImagen(rs.getString("imagen"));
            hito.setReto(rs.getString("reto"));
            lista.add(hito);
        }
        return lista;
    }

}
