package eduardo.romaguera.bl.entities.hito;

import java.util.Objects;

public class Hito {
    private int codigo;
    private String nombre;
    private double kilometros;
    private String descripcion;
    private String link;
    private String imagen;
    private String reto;

    public Hito() {
    }

    public Hito(String nombre, double kilometros, String descripcion, String link, String imagen, String reto) {
        this.nombre = nombre;
        this.kilometros = kilometros;
        this.descripcion = descripcion;
        this.link = link;
        this.imagen = imagen;
        this.reto = reto;
    }

    public Hito(int codigo, String nombre, double kilometros, String descripcion, String link, String imagen) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.kilometros = kilometros;
        this.descripcion = descripcion;
        this.link = link;
        this.imagen = imagen;
    }

    public Hito(int codigo, String nombre, double kilometros, String descripcion, String link, String imagen, String reto) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.kilometros = kilometros;
        this.descripcion = descripcion;
        this.link = link;
        this.imagen = imagen;
        this.reto = reto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getKilometros() {
        return kilometros;
    }

    public void setKilometros(double kilometros) {
        this.kilometros = kilometros;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getReto() {
        return reto;
    }

    public void setReto(String reto) {
        this.reto = reto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hito hito = (Hito) o;
        return getCodigo() == hito.getCodigo() && Double.compare(hito.getKilometros(), getKilometros()) == 0 && Objects.equals(getNombre(), hito.getNombre()) && Objects.equals(getDescripcion(), hito.getDescripcion()) && Objects.equals(getLink(), hito.getLink()) && Objects.equals(getImagen(), hito.getImagen()) && Objects.equals(getReto(), hito.getReto());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo(), getNombre(), getKilometros(), getDescripcion(), getLink(), getImagen(), getReto());
    }

    @Override
    public String toString() {
        return "Hito{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", kilometros=" + kilometros +
                ", descripcion='" + descripcion + '\'' +
                ", link='" + link + '\'' +
                ", imagen='" + imagen + '\'' +
                ", reto='" + reto + '\'' +
                '}';
    }
}
