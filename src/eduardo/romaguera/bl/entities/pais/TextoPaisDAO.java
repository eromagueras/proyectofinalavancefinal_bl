package eduardo.romaguera.bl.entities.pais;

import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;

import java.io.*;
import java.util.ArrayList;

public class TextoPaisDAO implements IPaisDAO {

    private final String PAISES_BD ="src\\eduardo\\romaguera\\dl\\txt\\PaisesBD.txt";

    public ArrayList<String> listarPaises() throws Exception {
    ArrayList<String> listaPaises = new ArrayList<>();
        try{
            FileReader reader = new FileReader(PAISES_BD);
            BufferedReader buffer = new BufferedReader(reader);
            String datos;
            String[] infoPais;
            String pais;

            while((datos = buffer.readLine()) != null){
                infoPais = datos.split(",");
                pais = new String(infoPais[0]);
                listaPaises.add(pais);
            }

        }catch(IOException e){
            e.printStackTrace();
        }
    return listaPaises;
    }

    @Override
    public ArrayList<Pais> listarPaises2() throws Exception {
        return null;
    }

    @Override
    public String registrarPais(Pais nuevo) throws Exception {
        return null;
    }

}