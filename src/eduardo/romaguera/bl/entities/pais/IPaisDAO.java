package eduardo.romaguera.bl.entities.pais;

import eduardo.romaguera.bl.entities.reto.Reto;

import java.util.ArrayList;

public interface IPaisDAO {

    ArrayList<String> listarPaises() throws Exception;
    ArrayList<Pais> listarPaises2() throws Exception;
    String registrarPais(Pais nuevo) throws Exception;
}
