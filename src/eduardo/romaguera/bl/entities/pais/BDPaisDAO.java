package eduardo.romaguera.bl.entities.pais;

import eduardo.romaguera.bl.entities.canton.Canton;
import eduardo.romaguera.bl.entities.usuario.Usuario;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDPaisDAO implements IPaisDAO {
    private String sqlQuery;

    @Override
    public ArrayList<String> listarPaises() throws Exception {
        ArrayList<String> lista = new ArrayList<>();

        sqlQuery="SELECT nombre FROM Pais";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            lista.add(rs.getString("nombre"));
        }
        return lista;
    }

    public ArrayList<Pais> listarPaises2() throws Exception {
        ArrayList<Pais> lista = new ArrayList<>();
        sqlQuery="SELECT * FROM Pais";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Pais pais = new Pais();
            pais.setNombre(rs.getString("nombre"));
            pais.setCodigo(rs.getString("codigo"));
            lista.add(pais);
        }
        return lista;
    }

    /**
     * Metodo que registra un Pais en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarPais(Pais nuevo) throws Exception {
        sqlQuery = "INSERT INTO Pais "+
            "(nombre)"+
            "VALUES('"+
                nuevo.getNombre()
//                +"', '"+

            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

}
