package eduardo.romaguera.bl.entities.pais;

import java.util.ArrayList;
import java.util.Objects;

public class Pais {
    private String nombre;
    private String codigo;

    public Pais() {
    }

    public Pais(String nombre) {
        this.nombre = nombre;
    }

    public Pais(String nombre, String codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pais pais = (Pais) o;
        return Objects.equals(getNombre(), pais.getNombre()) && Objects.equals(getCodigo(), pais.getCodigo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNombre(), getCodigo());
    }

    @Override
    public String toString() {
        return "Pais{" +
                "nombre='" + nombre + '\'' +
                ", codigo='" + codigo + '\'' +
                '}';
    }

    public String toStringSV() {
        return getNombre();
    }
}
