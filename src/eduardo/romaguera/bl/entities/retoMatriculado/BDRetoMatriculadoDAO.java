package eduardo.romaguera.bl.entities.retoMatriculado;


import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.metodoPago.MetodoPago;
import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.bl.entities.usuario.Usuario;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;

public class BDRetoMatriculadoDAO implements IRetoMatriculadoDAO {
    private String sqlQuery;
    private String sqlQuery2;

    /**
     * Metodo que registra un RetoMatriculado en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarRetoMatriculado(RetoMatriculado nuevo) throws Exception {
        sqlQuery =
                "INSERT INTO RetosMatriculados "+
                "(nombre, codigo, kilometros, pais, medalla, fotografia, descripcion, estado, avance)"+
                "VALUES('"+
                nuevo.getNombre()+"', '"+
                nuevo.getCodigo()+"', '"+
                nuevo.getKilometros()+"', '"+
                nuevo.getPais().toStringSV()+"', '"+
                nuevo.getMedalla()+"', '"+
                nuevo.getFotografia()+"', '"+
                nuevo.getDescripcion()+"', '"+
                nuevo.getEstado()+"', '"+
                nuevo.getAvance()
                +"');  ";
//                "INSERT INTO Grupo (codigoM, email)VALUES(last_insert_id(), + 'user')";
        sqlQuery2 =
            "INSERT INTO Grupo "+
            "(codigoM, email) "+
            "VALUES("+
            "(select max(codigoM) from RetosMatriculados), + '"+
            nuevo.getEmail()
            +"')";
        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery2);
        return "Good";
    }

    /**
     * Metodo que registra un amigo en un RetoMatriculado en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarAmigoRetoMatriculado(RetoMatriculado nuevo) throws Exception {
        sqlQuery2 =
                "INSERT INTO Grupo "+
                        "(codigoM, email) "+
                        "VALUES("+
                        nuevo.getCodigoM()+", '"+
                        nuevo.getEmail()
                        +"');  ";
        System.out.println(nuevo.getCodigoM());
        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery2);
        return "Good";
    }

    /**
     * Metodo que lista Retos Matriculados en la BD
     * @param
     * @return ArrayList<RetoMatriculado>
     * @throws Exception
     */
    public ArrayList<RetoMatriculado> listarRetosMatriculados(Usuario usuario) throws Exception {
        ArrayList<RetoMatriculado> lista = new ArrayList<>();
        String email = usuario.getEmail();
        sqlQuery = "SELECT * FROM RetosMatriculados join Grupo on RetosMatriculados.codigoM=Grupo.codigoM WHERE Grupo.email= '" + email + "'";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while (rs.next()) {
            RetoMatriculado reto = new RetoMatriculado();
            Atleta atleta = new Atleta();
            ArrayList<Atleta> atletas = new ArrayList<>();
            reto.setNombre(rs.getString("nombre"));
            reto.setCodigo(rs.getString("codigo"));
            reto.setKilometros(Double.valueOf(rs.getString("kilometros")));
            Pais pais = new Pais(rs.getString("pais"));
            reto.setPais(pais);
            reto.setMedalla(rs.getString("medalla"));
            reto.setFotografia(rs.getString("fotografia"));
            reto.setDescripcion(rs.getString("descripcion"));
            reto.setCodigoM(Integer.valueOf(rs.getString("codigoM")));
            reto.setEstado(rs.getString("estado"));
            reto.setAvance(Double.valueOf(rs.getString("avance")));
            reto.setEmail(rs.getString("email"));
            reto.setCodigoM(Integer.valueOf(rs.getString("codigoM")));
            lista.add(reto);
            sqlQuery2 = "SELECT Atletas.nombre, Atletas.email FROM Grupo join Atletas on Grupo.email = Atletas.email WHERE Grupo.codigoM= '" + reto.getCodigoM() + "'";
            ResultSet rs2 = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery2);
            while (rs2.next()) {
                atleta.setNombre(rs2.getString("nombre"));
                atleta.setEmail(rs2.getString("email"));
                atletas.add(atleta);
            }
            reto.setGrupo(atletas);

        }
        return lista;
    }
}
