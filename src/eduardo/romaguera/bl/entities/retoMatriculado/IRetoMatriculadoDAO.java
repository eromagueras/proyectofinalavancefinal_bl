package eduardo.romaguera.bl.entities.retoMatriculado;

import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.bl.entities.usuario.Usuario;

import java.util.ArrayList;

public interface IRetoMatriculadoDAO {
    String registrarRetoMatriculado(RetoMatriculado nuevo) throws Exception;
    String registrarAmigoRetoMatriculado(RetoMatriculado nuevo) throws Exception;
    ArrayList<RetoMatriculado> listarRetosMatriculados(Usuario usuario) throws Exception;

}
