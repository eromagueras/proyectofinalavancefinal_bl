package eduardo.romaguera.bl.entities.retoMatriculado;

import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.hito.Hito;
import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.reto.Reto;

import java.util.ArrayList;
import java.util.Objects;

public class RetoMatriculado extends Reto {
    private int codigoM;
    private ArrayList<Atleta> grupo;
    private String estado;
    private double avance;
    private ArrayList<String> avances;
    private String email;

    public RetoMatriculado() {
    }

    public RetoMatriculado(Reto reto) {
        super.setNombre(reto.getNombre());
        super.setCodigo(reto.getCodigo());
        super.setKilometros(reto.getKilometros());
        super.setPais(reto.getPais());
        super.setMedalla(reto.getMedalla());
        super.setFotografia(reto.getFotografia());
        super.setDescripcion(reto.getDescripcion());
        super.setHitos(reto.getHitos());
        this.setCodigoM(this.getCodigoM());
    }

    public RetoMatriculado(String nombre, String codigo, double kilometros, Pais pais, String medalla, String fotografia, String descripcion, ArrayList<Hito> hitos, int codigoM, ArrayList<Atleta> grupo, String estado, double avance, ArrayList<String> avances, String email) {
        super(nombre, codigo, kilometros, pais, medalla, fotografia, descripcion, hitos);
        this.codigoM = codigoM;
        this.grupo = grupo;
        this.estado = estado;
        this.avance = avance;
        this.avances = avances;
        this.email= email;
    }

    public RetoMatriculado(String nombre, String codigo, double kilometros, Pais pais, String medalla, String fotografia, String descripcion, ArrayList<Hito> hitos, int codigoM, ArrayList<Atleta> grupo, String estado, double avance, String email) {
        super(nombre, codigo, kilometros, pais, medalla, fotografia, descripcion, hitos);
        this.codigoM = codigoM;
        this.grupo = grupo;
        this.estado = estado;
        this.avance = avance;
        this.email= email;
    }

    public int getCodigoM() {
        return codigoM;
    }

    public void setCodigoM(int codigoM) {
        this.codigoM = codigoM;
    }

    public ArrayList<Atleta> getGrupo() {
        return grupo;
    }

    public void setGrupo(ArrayList<Atleta> grupo) {
        this.grupo = grupo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getAvance() {
        return avance;
    }

    public void setAvance(double avance) {
        this.avance = avance;
    }

    public ArrayList<String> getAvances() {
        return avances;
    }

    public void setAvances(ArrayList<String> avances) {
        this.avances = avances;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RetoMatriculado that = (RetoMatriculado) o;
        return getCodigoM() == that.getCodigoM() && getAvance() == that.getAvance() && Objects.equals(getGrupo(), that.getGrupo()) && Objects.equals(getEstado(), that.getEstado()) && Objects.equals(getAvances(), that.getAvances()) && Objects.equals(getEmail(), that.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCodigoM(), getGrupo(), getEstado(), getAvance(), getAvances(), getEmail());
    }

    @Override
    public String toString() {
        return "RetoMatriculado{" +
                "codigoM=" + codigoM +
                ", grupo=" + grupo +
                ", estado='" + estado + '\'' +
                ", avance=" + avance +
                ", avances=" + avances +
                ", email='" + email + '\'' +
                "} " + super.toString();
    }
}
