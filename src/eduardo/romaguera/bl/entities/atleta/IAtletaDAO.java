package eduardo.romaguera.bl.entities.atleta;

import eduardo.romaguera.bl.entities.usuario.Usuario;

public interface IAtletaDAO {
    String registrarAtleta(Atleta nuevoAtleta) throws Exception;
}
