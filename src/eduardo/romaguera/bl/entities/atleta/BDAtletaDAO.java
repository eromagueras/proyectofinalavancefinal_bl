package eduardo.romaguera.bl.entities.atleta;


import eduardo.romaguera.dl.db.ConectorBD;

public class BDAtletaDAO implements IAtletaDAO {
    private String sqlQuery;

    /**
     * Metodo que registra un Atleta en la BD
     * @param nuevoAtleta
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarAtleta(Atleta nuevoAtleta) throws Exception {
//        sqlQuery = "INSERT INTO Atletas (nombre, apellidos, identificacion, pais) VALUES('Eduardo', 'Romaguera', 1111, 'Costa Rica')";
        sqlQuery = "INSERT INTO Atletas "+
            "(nombre, apellidos, identificacion, pais, email, contrasena, tipoUsuario, genero, fechaNacimiento, edad, direccionPais, direccionOtrosDatos, direccionProvincia, direccionCanton, direccionDistrito, avatar)"+
            "VALUES('"+
            nuevoAtleta.getNombre()+"', '"+
            nuevoAtleta.getApellidos()+"', '"+
            nuevoAtleta.getIdentificacion()+"', '"+
            nuevoAtleta.getPais().toStringSV()+"', '"+
            nuevoAtleta.getEmail()+"', '"+
            nuevoAtleta.getContrasena()+"', '"+
            "Atleta"+"', '"+
            nuevoAtleta.getGenero()+"', '"+
            nuevoAtleta.getFechaNacimiento()+"', '"+
            nuevoAtleta.getEdad()+"', '"+
            nuevoAtleta.getDireccionPais().toStringSV()+"', '"+
            nuevoAtleta.getDireccionOtrosDatos()+"', '"+
            nuevoAtleta.getDireccionProvincia().toStringSV()+"', '"+
            nuevoAtleta.getDireccionCanton().toStringSV()+"', '"+
            nuevoAtleta.getDireccionDistrito().toStringSV()+"', '"+
            nuevoAtleta.getAvatar()
            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }

}
