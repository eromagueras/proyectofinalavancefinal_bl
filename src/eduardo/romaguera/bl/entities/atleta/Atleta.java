package eduardo.romaguera.bl.entities.atleta;

import eduardo.romaguera.bl.entities.canton.Canton;
import eduardo.romaguera.bl.entities.distrito.Distrito;
import eduardo.romaguera.bl.entities.metodoPago.MetodoPago;
import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.provincia.Provincia;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.bl.entities.usuario.Usuario;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

public class Atleta extends Usuario {
    private String genero;
    private LocalDate fechaNacimiento;
    private int edad;
    private Pais direccionPais;
    private String direccionOtrosDatos;
    private Provincia direccionProvincia;
    private Canton direccionCanton;
    private Distrito direccionDistrito;
    private ArrayList<MetodoPago> metodoPago;
    private ArrayList<Reto> retoMatriculado;

    public Atleta() {
    }

    public Atleta(String nombre, String apellidos, String identificacion, Pais pais, String email, String contrasena, String genero, LocalDate fechaNacimiento, int edad, Pais direccionPais, String direccionOtrosDatos, Provincia direccionProvincia, Canton direccionCanton, Distrito direccionDistrito) {
        super(nombre, apellidos, identificacion, pais, email, contrasena, "Atleta");
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.direccionPais = direccionPais;
        this.direccionOtrosDatos = direccionOtrosDatos;
        this.direccionProvincia = direccionProvincia;
        this.direccionCanton = direccionCanton;
        this.direccionDistrito = direccionDistrito;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Pais getDireccionPais() {
        return direccionPais;
    }

    public void setDireccionPais(Pais direccionPais) {
        this.direccionPais = direccionPais;
    }

    public String getDireccionOtrosDatos() {
        return direccionOtrosDatos;
    }

    public void setDireccionOtrosDatos(String direccionOtrosDatos) {
        this.direccionOtrosDatos = direccionOtrosDatos;
    }

    public Provincia getDireccionProvincia() {
        return direccionProvincia;
    }

    public void setDireccionProvincia(Provincia direccionProvincia) {
        this.direccionProvincia = direccionProvincia;
    }

    public Canton getDireccionCanton() {
        return direccionCanton;
    }

    public void setDireccionCanton(Canton direccionCanton) {
        this.direccionCanton = direccionCanton;
    }

    public Distrito getDireccionDistrito() {
        return direccionDistrito;
    }

    public void setDireccionDistrito(Distrito direccionDistrito) {
        this.direccionDistrito = direccionDistrito;
    }

    public ArrayList<MetodoPago> getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(ArrayList<MetodoPago> metodoPago) {
        this.metodoPago = metodoPago;
    }

    public ArrayList<Reto> getRetoMatriculado() {
        return retoMatriculado;
    }

    public void setRetoMatriculado(ArrayList<Reto> retoMatriculado) {
        this.retoMatriculado = retoMatriculado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Atleta atleta = (Atleta) o;
        return Objects.equals(getGenero(), atleta.getGenero()) && Objects.equals(getFechaNacimiento(), atleta.getFechaNacimiento()) && Objects.equals(getEdad(), atleta.getEdad()) && Objects.equals(getDireccionPais(), atleta.getDireccionPais()) && Objects.equals(getDireccionOtrosDatos(), atleta.getDireccionOtrosDatos()) && Objects.equals(getDireccionProvincia(), atleta.getDireccionProvincia()) && Objects.equals(getDireccionCanton(), atleta.getDireccionCanton()) && Objects.equals(getDireccionDistrito(), atleta.getDireccionDistrito()) && Objects.equals(getMetodoPago(), atleta.getMetodoPago()) && Objects.equals(getRetoMatriculado(), atleta.getRetoMatriculado());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getGenero(), getFechaNacimiento(), getEdad(), getDireccionPais(), getDireccionOtrosDatos(), getDireccionProvincia(), getDireccionCanton(), getDireccionDistrito(), getMetodoPago(), getRetoMatriculado());
    }

    @Override
    public String toString() {
        return "Atleta{" +
                "genero='" + genero + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", direccionPais=" + direccionPais +
                ", direccionOtrosDatos='" + direccionOtrosDatos + '\'' +
                ", direccionProvincia=" + direccionProvincia +
                ", direccionCanton=" + direccionCanton +
                ", direccionDistrito=" + direccionDistrito +
                ", metodoPago=" + metodoPago +
                ", retoMatriculado=" + retoMatriculado +
                "} " + super.toString();
    }

    public String toStringCSV(){
        return super.getNombre() + ","+ super.getApellidos() + ","+ super.getIdentificacion() + ","+ super.getPais().getNombre() + ","+ super.getEmail() + ","+ super.getContrasena()+ ","+ genero + ","+ fechaNacimiento + ","+ edad + ","+ direccionPais.getNombre() + ","+ direccionOtrosDatos + ","+ direccionProvincia.getNombre()+","+direccionCanton.getNombre()+ ","+direccionDistrito.getNombre();
    }
}
