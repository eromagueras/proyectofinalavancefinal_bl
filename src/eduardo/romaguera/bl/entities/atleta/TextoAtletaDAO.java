package eduardo.romaguera.bl.entities.atleta;

import eduardo.romaguera.bl.entities.usuario.Usuario;
import eduardo.romaguera.dl.txt.Conector;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TextoAtletaDAO implements IAtletaDAO {

    private final String ATLETAS_BD ="src\\eduardo\\romaguera\\dl\\txt\\AtletasBD.txt";


    public String registrarAtleta(Atleta nuevoAtleta) throws Exception {
        try{
            FileWriter writer = new FileWriter(ATLETAS_BD,true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(nuevoAtleta.toStringCSV());
            buffer.newLine();
            buffer.close();
            return "Good";
        }catch(IOException e){
            e.printStackTrace();
        }

        return "Error";
    }

}