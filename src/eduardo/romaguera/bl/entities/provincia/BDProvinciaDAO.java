package eduardo.romaguera.bl.entities.provincia;

import eduardo.romaguera.bl.entities.canton.Canton;
import eduardo.romaguera.bl.entities.canton.ICantonDAO;
import eduardo.romaguera.dl.db.ConectorBD;

import java.sql.ResultSet;
import java.util.ArrayList;

public class BDProvinciaDAO implements IProvinciaDAO {
    private String sqlQuery;

    /**
     * Metodo que lista Provincias en la BD
     * @param
     * @return ArrayList<String>
     * @throws Exception
     */
    @Override
    public ArrayList<String> listarProvincias() throws Exception {
        ArrayList<String> lista = new ArrayList<>();

        sqlQuery="SELECT nombre FROM Provincias";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            lista.add(rs.getString("nombre"));
        }
        return lista;
    }

    /**
     * Metodo que lista Provincias en la BD
     * @param
     * @return ArrayList<Provincia>
     * @throws Exception
     */
    @Override
    public ArrayList<Provincia> listarProvincias2() throws Exception {
        ArrayList<Provincia> lista = new ArrayList<>();
        sqlQuery="SELECT * FROM Provincias";
        ResultSet rs = ConectorBD.getConnector().ejecutarQueryGET(sqlQuery);

        while(rs.next()){
            Provincia provincia = new Provincia();
            provincia.setNombre(rs.getString("nombre"));
            provincia.setCodigo(rs.getString("codigo"));
            lista.add(provincia);
        }
        return lista;
    }

    /**
     * Metodo que registra una Provincia en la BD
     * @param nuevo
     * @return String "Good"
     * @throws Exception
     */
    @Override
    public String registrarProvincia(Provincia nuevo) throws Exception {
        sqlQuery = "INSERT INTO Provincias "+
            "(nombre)"+
            "VALUES('"+
                nuevo.getNombre()
//                +"', '"+

            +"')";

        ConectorBD.getConnector().ejecutarQuerySET(sqlQuery);
        return "Good";
    }
}
