package eduardo.romaguera.bl.entities.provincia;

import eduardo.romaguera.bl.entities.pais.Pais;

import java.util.ArrayList;

public interface IProvinciaDAO {
    ArrayList<String> listarProvincias() throws Exception;
    String registrarProvincia(Provincia nuevo) throws Exception;
    ArrayList<Provincia> listarProvincias2() throws Exception;
}