package eduardo.romaguera.bl.entities.provincia;

import java.util.Objects;

public class Provincia {
    private String codigo;
    private String nombre;

    public Provincia() {
    }

    public Provincia(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public Provincia(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Provincia provincia = (Provincia) o;
        return Objects.equals(getCodigo(), provincia.getCodigo()) && Objects.equals(getNombre(), provincia.getNombre());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo(), getNombre());
    }

    @Override
    public String toString() {
        return "Provincia{" +
                "codigo='" + codigo + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public String toStringSV() {
        return getNombre();
    }
}
