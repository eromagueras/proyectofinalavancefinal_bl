package eduardo.romaguera.bl.entities.provincia;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TextoProvinciaDAO implements IProvinciaDAO {

    private final String PROVINCIAS_BD ="src\\eduardo\\romaguera\\dl\\txt\\ProvinciasBD.txt";

    public ArrayList<String> listarProvincias() throws Exception {
    ArrayList<String> listaProvincias = new ArrayList<>();
        try{
            FileReader reader = new FileReader(PROVINCIAS_BD);
            BufferedReader buffer = new BufferedReader(reader);
            String datos;
            String[] infoProvincia;
            String Provincia;

            while((datos = buffer.readLine()) != null){
                infoProvincia = datos.split(",");
                Provincia = new String(infoProvincia[0]);
                listaProvincias.add(Provincia);
            }

        }catch(IOException e){
            e.printStackTrace();
        }
    return listaProvincias;
    }

    @Override
    public String registrarProvincia(Provincia nuevo) throws Exception {
        return null;
    }

    @Override
    public ArrayList<Provincia> listarProvincias2() throws Exception {
        return null;
    }

}