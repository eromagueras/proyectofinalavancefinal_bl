package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.usuario.Usuario;
import eduardo.romaguera.dao.DAOFactory;
import eduardo.romaguera.bl.entities.usuario.IUsuarioDAO;

import java.util.ArrayList;

public class UsuarioGestor {
    private IUsuarioDAO IUsuarioDAO;
    private DAOFactory DAOFactory;

    public UsuarioGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IUsuarioDAO = DAOFactory.getUsuarioDAO();
    }

    /**
     * Método para verificar que el usuario buscado existe, login
     * @param usuarioBuscado Usuario
     * @return Usuario Usuario
     * @throws Exception
     */
    public Usuario verificarAcceso(Usuario usuarioBuscado) throws Exception {
        Usuario Usuario;
        Usuario = IUsuarioDAO.verificarAcceso(usuarioBuscado);
        return Usuario;
    }
    /**
     * Método para verificar que existe un administrador en el sistema
     * @param
     * @return String con info de transacción
     * @throws Exception
     */
    public ArrayList<Usuario> verificarAdministrador() throws Exception {
        ArrayList<Usuario> Usuario;
        Usuario = IUsuarioDAO.listarAdministradores();
        return Usuario;
    }
}
