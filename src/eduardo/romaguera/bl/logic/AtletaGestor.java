package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;
import eduardo.romaguera.dao.DAOFactory;

public class AtletaGestor {
    private IAtletaDAO IAtletaDAO;
    private DAOFactory DAOFactory;

    public AtletaGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IAtletaDAO = DAOFactory.getAtletaDAO();
    }

    /**
     * Método para registrar un nuevo atleta en la BD
     * @param nuevoAtleta
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarAtleta(Atleta nuevoAtleta) throws Exception {
        String Registro;
        Registro = IAtletaDAO.registrarAtleta(nuevoAtleta);
        return Registro;
    }
}
