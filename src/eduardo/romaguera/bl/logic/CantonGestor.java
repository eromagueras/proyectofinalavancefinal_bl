package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.canton.Canton;
import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.dao.DAOFactory;
import eduardo.romaguera.bl.entities.canton.ICantonDAO;

import java.util.ArrayList;

public class CantonGestor {
    private ICantonDAO ICantonDAO;
    private DAOFactory DAOFactory;

    public CantonGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        ICantonDAO = DAOFactory.getCantonDAO();
    }

    /**
     * Método para listar los cantones de la BD
     * @param
     * @return ArrayList<Canton>
     * @throws Exception
     */
    public ArrayList<Canton> listarCantones() throws Exception {
        ArrayList<Canton> listaCantones = new ArrayList<>();
        listaCantones = ICantonDAO.listarCantones();
        return listaCantones;
    }

    /**
     * Método para registrar un nuevo cantón en la BD
     * @param nuevo Cantón
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarCanton(Canton nuevo) throws Exception {
        String Registro;
        Registro = ICantonDAO.registrarCanton(nuevo);
        return Registro;
    }
}
