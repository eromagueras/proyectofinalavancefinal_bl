package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.pais.IPaisDAO;
import eduardo.romaguera.bl.entities.pais.Pais;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class PaisGestor {
    private IPaisDAO IPaisDAO;
    private DAOFactory DAOFactory;

    public PaisGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IPaisDAO = DAOFactory.getPaisDAO();
    }

    /**
     * Método para listar los paises en la BD
     * @param
     * @return ArrayList<String>
     * @throws Exception
     */
    public ArrayList<String> listarPaises() throws Exception {
        ArrayList<String> listaPaises = new ArrayList<>();
        listaPaises = IPaisDAO.listarPaises();
        return listaPaises;
    }

    /**
     * Método para listar los paises en la BD
     * @param
     * @return ArrayList<Pais>
     * @throws Exception
     */
    public ArrayList<Pais> listarPaises2() throws Exception {
        ArrayList<Pais> listaPaises = new ArrayList<>();
        listaPaises = IPaisDAO.listarPaises2();
        return listaPaises;
    }

    /**
     * Método para registrar un nuevo Pais en la BD
     * @param nuevoPais Pais
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarPais(Pais nuevoPais) throws Exception {
        String Registro;
        Registro = IPaisDAO.registrarPais(nuevoPais);
        return Registro;
    }
}
