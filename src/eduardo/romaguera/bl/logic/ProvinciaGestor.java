package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.provincia.IProvinciaDAO;
import eduardo.romaguera.bl.entities.provincia.Provincia;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class ProvinciaGestor {
    private IProvinciaDAO IProvinciaDAO;
    private DAOFactory DAOFactory;

    public ProvinciaGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IProvinciaDAO = DAOFactory.getProvinciaDAO();
    }

    /**
     * Método para listar las provincias en la BD
     * @param
     * @return ArrayList<String>
     * @throws Exception
     */
    public ArrayList<String> listarProvincias() throws Exception {
        ArrayList<String> listaProvincias = new ArrayList<>();
        listaProvincias = IProvinciaDAO.listarProvincias();
        return listaProvincias;
    }

    /**
     * Método para listar las provincias en la BD
     * @param
     * @return ArrayList<Provincia>
     * @throws Exception
     */
    public ArrayList<Provincia> listarProvincias2() throws Exception {
        ArrayList<Provincia> listaProvincias = new ArrayList<>();
        listaProvincias = IProvinciaDAO.listarProvincias2();
        return listaProvincias;
    }

    /**
     * Método para registrar una nueva Provincia en la BD
     * @param nuevaProvincia Provincia
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarProvincia(Provincia nuevaProvincia) throws Exception {
        String Registro;
        Registro = IProvinciaDAO.registrarProvincia(nuevaProvincia);
        return Registro;
    }
}
