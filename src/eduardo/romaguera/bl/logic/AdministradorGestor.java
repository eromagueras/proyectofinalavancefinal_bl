package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.administrador.Administrador;
import eduardo.romaguera.bl.entities.administrador.IAdministradorDAO;
import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;
import eduardo.romaguera.dao.DAOFactory;

public class AdministradorGestor {
    private IAdministradorDAO IAdministradorDAO;
    private DAOFactory DAOFactory;

    public AdministradorGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IAdministradorDAO = DAOFactory.getAdministradorDAO();
    }

    /**
     * Método para registrar el administrador en el sistema
     * @param nuevo Objeto admnistrador
     * @return String con info de la transacción
     * @throws Exception
     */
    public String registrarAdministrador(Administrador nuevo) throws Exception {
        String Registro;
        Registro = IAdministradorDAO.registrarAdministrador(nuevo);
        return Registro;
    }
}
