package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.canton.Canton;
import eduardo.romaguera.bl.entities.distrito.Distrito;
import eduardo.romaguera.bl.entities.distrito.IDistritoDAO;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class DistritoGestor {
    private IDistritoDAO IDistritoDAO;
    private DAOFactory DAOFactory;

    public DistritoGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IDistritoDAO = DAOFactory.getDistritoDAO();
    }

    /**
     * Método para listar un nuevo avance en la BD
     * @param
     * @return ArrayList<Distrito>
     * @throws Exception
     */
    public ArrayList<Distrito> listarDistritos() throws Exception {
        ArrayList<Distrito> listaDistritos = new ArrayList<>();
        listaDistritos = IDistritoDAO.listarDistritos();
        return listaDistritos;
    }

    /**
     * Método para registrar un nuevo Distrito en la BD
     * @param nuevo Distrito
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarDistrito(Distrito nuevo) throws Exception {
        String Registro;
        Registro = IDistritoDAO.registrarDistrito(nuevo);
        return Registro;
    }

}
