package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.bl.entities.retoMatriculado.IRetoMatriculadoDAO;
import eduardo.romaguera.bl.entities.retoMatriculado.RetoMatriculado;
import eduardo.romaguera.bl.entities.usuario.Usuario;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class RetoMatriculadoGestor {
    private IRetoMatriculadoDAO IRetoMatriculadoDAO;
    private DAOFactory DAOFactory;

    public RetoMatriculadoGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IRetoMatriculadoDAO = DAOFactory.getRetoMatriculadoDAO();
    }

    /**
     * Método para registrar un RetoMatriculado en la BD
     * @param nuevoReto RetoMatriculado
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarRetoMatriculado(RetoMatriculado nuevoReto) throws Exception {
        String Registro;
        Registro = IRetoMatriculadoDAO.registrarRetoMatriculado(nuevoReto);
        return Registro;
    }


    /**
     * Método para registrar un amigo a un RetoMatriculado en la BD
     * @param nuevoReto RetoMatriculado
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarAmigoRetoMatriculado(RetoMatriculado nuevoReto) throws Exception {
        String Registro;
        Registro = IRetoMatriculadoDAO.registrarAmigoRetoMatriculado(nuevoReto);
        return Registro;
    }

    /**
     * Método para listar las Retos Matriculados en la BD
     * @param usuario Usuario
     * @return ArrayList<RetoMatriculado>
     * @throws Exception
     */
    public ArrayList<RetoMatriculado> listarRetosMatriculados(Usuario usuario) throws Exception {
        ArrayList<RetoMatriculado> lista = new ArrayList<>();
            lista = IRetoMatriculadoDAO.listarRetosMatriculados(usuario);
        return lista;
    }

}
