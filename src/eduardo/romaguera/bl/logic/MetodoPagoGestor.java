package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.metodoPago.MetodoPago;
import eduardo.romaguera.bl.entities.metodoPago.IMetodoPagoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.bl.entities.usuario.Usuario;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class MetodoPagoGestor {
    private IMetodoPagoDAO IMetodoPagoDAO;
    private DAOFactory DAOFactory;

    public MetodoPagoGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IMetodoPagoDAO = DAOFactory.getMetodoPagoDAO();
    }
    /**
     * Método para registrar un nuevo metodo de pago en la BD
     * @param nuevo MetodoPago
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarMetodoPago(MetodoPago nuevo) throws Exception {
        String Registro;
        Registro = IMetodoPagoDAO.registrarMetodoPago(nuevo);
        return Registro;
    }

    /**
     * Método para listar los Metodos de pago en la BD
     * @param Usuario usuario dueño del método de pago
     * @return ArrayList<MetodoPago>
     * @throws Exception
     */
        public ArrayList<MetodoPago> listarMetodoPago(Usuario usuario) throws Exception {
        ArrayList<MetodoPago> lista = new ArrayList<>();
            lista = IMetodoPagoDAO.listarMetodoPago(usuario);
        return lista;
    }

}