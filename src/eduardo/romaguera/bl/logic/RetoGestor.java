package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.atleta.Atleta;
import eduardo.romaguera.bl.entities.atleta.IAtletaDAO;
import eduardo.romaguera.bl.entities.distrito.Distrito;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class RetoGestor {
    private IRetoDAO IRetoDAO;
    private DAOFactory DAOFactory;

    public RetoGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IRetoDAO = DAOFactory.getRetoDAO();
    }

    /**
     * Método para registrar un nuevo Reto en la BD
     * @param nuevoReto Reto
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarReto(Reto nuevoReto) throws Exception {
        String Registro;
        Registro = IRetoDAO.registrarReto(nuevoReto);
        return Registro;
    }

    /**
     * Método para listar las Retos en la BD
     * @param
     * @return ArrayList<Reto>
     * @throws Exception
     */
    public ArrayList<Reto> listarRetos() throws Exception {
        ArrayList<Reto> lista = new ArrayList<>();
            lista = IRetoDAO.listarRetos();
        return lista;
    }

}
