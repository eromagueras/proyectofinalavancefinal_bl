package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.email.Email;
import eduardo.romaguera.bl.entities.email.EmailUtil;
import eduardo.romaguera.bl.entities.email.IEmail;

public class EmailGestor {
    private IEmail Cartero;

    public EmailGestor() {
        Cartero = new EmailUtil();
    }

    /**
     * Método para enviar un correo electrónico
     * @param email Email
     * @return String con info de transacción
     * @throws Exception
     */
    public String sendEmail(Email email) throws Exception {
        String Envio;
        Cartero.sendEmail(email);
        return "Good";
    }
}
