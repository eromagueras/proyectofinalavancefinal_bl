package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.Avance.Avance;
import eduardo.romaguera.bl.entities.Avance.IAvanceDAO;
import eduardo.romaguera.bl.entities.hito.IHitoDAO;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class AvanceGestor {
    private IAvanceDAO IAvanceDAO;
    private DAOFactory DAOFactory;

    public AvanceGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IAvanceDAO = DAOFactory.getAvanceDAO();
    }

    /**
     * Método para registrar un nuevo avance en la BD
     * @param nuevo Avance
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarAvance(Avance nuevo) throws Exception {
        String Registro;
        Registro = IAvanceDAO.registrarAvance(nuevo);
        return Registro;
    }

    /**
     * Método para listar los avances de la BD
     * @param
     * @return ArrayList<Avance>
     * @throws Exception
     */
        public ArrayList<Avance> listarAvances() throws Exception {
        ArrayList<Avance> lista = new ArrayList<>();
            lista = IAvanceDAO.listarAvances();
        return lista;
    }

}
