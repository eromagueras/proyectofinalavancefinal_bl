package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.actividadDeportiva.ActividadDeportiva;
import eduardo.romaguera.bl.entities.actividadDeportiva.IActividadDeportivaDAO;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class ActividadDeportivaGestor {
    private IActividadDeportivaDAO IActividadDeportivaDAO;
    private DAOFactory DAOFactory;


    public ActividadDeportivaGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IActividadDeportivaDAO = DAOFactory.getActividadDeportivaDAO();
    }

    /**
     * Método para registrar una nueva actividad deportiva en la BD
     * @param nuevaActividadDeportiva
     * @return String con info sobre éxito de la transacción
     * @throws Exception
     */
    public String registrarActividadDeportiva(ActividadDeportiva nuevaActividadDeportiva) throws Exception {
        String Registro;
        Registro = IActividadDeportivaDAO.registrarActividadDeportiva(nuevaActividadDeportiva);
        return Registro;
    }

    /**
     * Método para listar las actividades deportivas en la BD
     * @return ArrayList<ActividadDeportiva>
     * @throws Exception
     */
    public ArrayList<ActividadDeportiva> listarActividadDeportiva() throws Exception {
        ArrayList<ActividadDeportiva> lista = new ArrayList<>();
        lista = IActividadDeportivaDAO.listarActividadDeportiva();
        return lista;
    }
}
