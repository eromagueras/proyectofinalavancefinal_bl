package eduardo.romaguera.bl.logic;

import eduardo.romaguera.bl.entities.hito.Hito;
import eduardo.romaguera.bl.entities.hito.IHitoDAO;
import eduardo.romaguera.bl.entities.reto.IRetoDAO;
import eduardo.romaguera.bl.entities.reto.Reto;
import eduardo.romaguera.dao.DAOFactory;

import java.util.ArrayList;

public class HitoGestor {
    private IHitoDAO IHitoDAO;
    private DAOFactory DAOFactory;

    public HitoGestor() {
        DAOFactory = DAOFactory.getDaoFactory(2);
        IHitoDAO = DAOFactory.getHitoDAO();
    }

    /**
     * Método para registrar un nuevo hito en la BD
     * @param nuevo Hito
     * @return String con info de transacción
     * @throws Exception
     */
    public String registrarHito(Hito nuevo) throws Exception {
        String Registro;
        Registro = IHitoDAO.registrarHito(nuevo);
        return Registro;
    }

    /**
     * Método para listar los hitos de la BD
     * @param
     * @return ArrayList<Hito>
     * @throws Exception
     */
    public ArrayList<Hito> listarHitos() throws Exception {
        ArrayList<Hito> lista = new ArrayList<>();
            lista = IHitoDAO.listarHitos();
        return lista;
    }

}
