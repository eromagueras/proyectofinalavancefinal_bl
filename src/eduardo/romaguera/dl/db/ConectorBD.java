package eduardo.romaguera.dl.db;

import eduardo.romaguera.util.Utils;

public class ConectorBD {
    private static AccesoBD conexionBD = null;

    public static AccesoBD getConnector() throws Exception {

        String[] infoBD = Utils.getProperties();
        String URL = infoBD[0] + "//" + infoBD[1] + "/" + infoBD[2] + infoBD[3];
        String user = infoBD[4];
        String password = infoBD[5];

        if (conexionBD == null) {
            conexionBD = new AccesoBD(URL, user, password);
        }
        return conexionBD;
    }


//        String URL =
//            "jdbc:mysql://localhost/werun?useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true";
////            "jdbc:sqlite:werun.db";
////          "jdbc:sqlserver://localhost:1433;database=werun;user=user;password=qwerty"; SQLSERVER
//        String user ="user";
//        String password = "qwerty";
//        conexionBD = new AccesoBD(URL, user, password);
//        return conexionBD;
//    }



}
