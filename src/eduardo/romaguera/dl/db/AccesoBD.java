package eduardo.romaguera.dl.db;

import java.sql.*;

public class AccesoBD {

    private Connection conn = null;
    private Statement stmt = null;

    public AccesoBD(String URL, String user, String password) throws Exception {
        conn = DriverManager.getConnection(URL,user,password);
    }


//Metodo para INSERT,UPDATE,DELETE
    public void ejecutarQuerySET(String query) throws Exception{
        stmt = conn.createStatement();
        stmt.executeUpdate(query);
    }

//Metodo para GET o Selects
    public ResultSet ejecutarQueryGET(String query) throws Exception{
        ResultSet rs = null;
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        return rs;
    }
}
